﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Graduate_Placement
{
    public partial class Main_Menu : Form
    {
        public static SQLiteConnection conn;  
        public Main_Menu()
        {
            InitializeComponent();
            SQLiteConnection.CreateFile("MyDatabase.test1");
            conn = new SQLiteConnection("Data Source=MyDatabase.test1;Version=3;");
            conn.Open();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Data_Entry form = new Data_Entry();
            form.Show(this);
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Report_Editing form = new Report_Editing();
            form.Show(this);
            Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            General_Report form = new General_Report();
            form.Show(this);
            Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Summary_Lists form = new Summary_Lists();
            form.Show(this);
            Hide();
        }

        private void Main_Menu_Load(object sender, EventArgs e)
        {


        }

        private void Main_Menu_FormClosing(object sender, EventArgs e)
        {
            conn.Close();
        }

    }
}
