﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graduate_Placement
{
    public partial class Startmenu : Form
    {
        public Startmenu()
        {
            InitializeComponent();
        }

        private void Openfrm(object sender, EventArgs e)
        {
            Open_Project_Form frm_1 = new Open_Project_Form();
            frm_1.Show(this);
            this.Hide();
        }

        private void Newfrm(object sender, EventArgs e)
        {
            New_Project_Form frm_2 = new New_Project_Form();
            frm_2.Show(this);
            this.Hide();
        }

    }
}
